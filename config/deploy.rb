# config valid only for Capistrano 3.1
lock '3.1.0'

set :application, 'campo'
set :repo_url, 'https://bitbucket.org/cbasic7/campo.git'
set :deploy_to, -> { "/var/www/#{fetch(:application)}" }
set :rails_env, 'production'

set :linked_files, %w{config/database.yml config/config.yml config/secrets.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/uploads}

set :puma_conf, "config/puma.rb"

namespace :deploy do
  desc "Starting deployment"
  task :initial do
    on roles(:web) do
      before 'deploy:restart', 'puma:start'
    end
  end

  desc "Restart unicorn and resque"
  task :restart do
    invoke 'deploy:resque:restart'
  end
  after :publishing, :restart

 namespace :resque do
    %w( start stop restart ).each do |action|
      desc "#{action} resque worker"
      task action do
        on roles(:web) do
          execute :service, :resque, action
        end
      end
    end
  end
end
