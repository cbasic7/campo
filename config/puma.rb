environment 'production'
daemonize true
worker 2
threads 1, 6
bind "unix:///var/www/campo/shared/sockets/campo.sock"
preload_app!
